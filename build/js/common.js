

$(document).ready(function(){

	// first slider
	$('.services_items').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		lazyLoad: 'ondemand',
		// dots: true,
		fade: true,
		// asNavFor: '.test2'
		touchMove: false,
		swipe: false,
		// asNavFor: '.services_links_list',
		responsive: [{
			breakpoint: 992,
			settings: {
				swipeToSlide: true,
				touchMove: true,
				swipe: true,
				asNavFor: '.services_links_list',
				fade: false

			}
		}
		]

	});

	$('.services_links_list').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		asNavFor: '.services_items',
		centerMode: false,
		focusOnSelect: true,
		lazyLoad: 'ondemand',
		vertical: false,
		arrows: false,
		swipeToSlide: true,
		infinite: false,
		responsive: [{
			breakpoint: 992,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
				centerMode: true,
				touchMove: true,
				infinite: true
				// centerPadding: '0'
			}
		}
		]
	});







	$('.events_slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		// dots: true,
		fade: true,
		lazyLoad: 'ondemand',
		// asNavFor: '.test2'
		touchMove: false,
		swipe: true,
		asNavFor: '.slider_miniatures',
		responsive: [{
			breakpoint: 768,
			settings: {
				fade: false
			}
		}
		]
	});

	$('.slider_miniatures').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: '.events_slider',
		// dots: true,
		// centerMode: true,
		focusOnSelect: true,
		vertical: true,
		lazyLoad: 'ondemand',
		arrows: false,
		swipeToSlide: true,
		verticalSwiping: true,
		responsive: [
			{
			breakpoint: 768,
			settings: {
				slidesToShow: 5,
				slidesToScroll: 1,
				centerMode: true,
				vertical: false,
				centerPadding: '0',
				infinite: true,
				verticalSwiping: false
			}
		},

			{
				breakpoint: 570,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					centerMode: true,
					vertical: false,
					centerPadding: '0',
					infinite: true,
					verticalSwiping: false
				}
			}
		]
		// touchMove: false.,
	});








	// games_slider
	$('.games_slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		touchMove: false,
		lazyLoad: 'ondemand',
		swipe: false,
		responsive: [{
			breakpoint: 992,
			settings: {
				fade: false
			}
		}
		]

	});
	$('.games_miniatures').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		asNavFor: '.games_slider',
		centerMode: false,
		focusOnSelect: true,
		lazyLoad: 'ondemand',
		vertical: false,
		arrows: false,
		swipeToSlide: true,
		infinite: false,
		responsive: [{
			breakpoint: 992,
			settings: {
				appendArrows: '.games_slider_area .games_miniatures_title',
				slidesToShow: 3,
				slidesToScroll: 1,
				centerMode: true,
				touchMove: true,
				infinite: true,
				arrows: true,
				centerPadding: '0',
				prevArrow: '<div  class="g_slick-prev"><span class="icon arrow"></span></div>',
				nextArrow: '<div  class="g_slick-next"><span class="icon arrow"></span></div>',
			}
		}
		]
	});

	$('.gallery_m').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		appendDots: '.dots_m',
		centerMode: false,
		lazyLoad: 'ondemand',
		focusOnSelect: true,
		vertical: false,
		arrows: true,
		swipeToSlide: true,
		prevArrow: '<div  class="slick-prev"><span class="icon arrow"></span></div>',
		nextArrow: '<div  class="slick-next"><span class="icon arrow"></span></div>',
		infinite: true
	});

	$('.gallery_i').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		appendDots: '.dots_i',
		centerMode: false,
		lazyLoad: 'ondemand',
		focusOnSelect: true,
		vertical: false,
		arrows: true,
		swipeToSlide: true,
		prevArrow: '<div  class="slick-prev"><span class="icon arrow"></span></div>',
		nextArrow: '<div  class="slick-next"><span class="icon arrow"></span></div>',
		infinite: true
	});

	$('.gallery_v').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		appendDots: '.dots_v',
		centerMode: false,
		lazyLoad: 'ondemand',
		focusOnSelect: true,
		vertical: false,
		arrows: true,
		swipeToSlide: true,
		prevArrow: '<div  class="slick-prev"><span class="icon arrow"></span></div>',
		nextArrow: '<div  class="slick-next"><span class="icon arrow"></span></div>',
		infinite: true
	});

	$('.gallery_r').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		appendDots: '.dots_r',
		centerMode: false,
		lazyLoad: 'ondemand',
		focusOnSelect: true,
		vertical: false,
		arrows: true,
		swipeToSlide: true,
		prevArrow: '<div  class="slick-prev"><span class="icon arrow"></span></div>',
		nextArrow: '<div  class="slick-next"><span class="icon arrow"></span></div>',
		infinite: true
	});

	$('.gallery_l').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		appendDots: '.dots_l',
		centerMode: false,
		lazyLoad: 'ondemand',
		focusOnSelect: true,
		vertical: false,
		arrows: true,
		swipeToSlide: true,
		prevArrow: '<div  class="slick-prev"><span class="icon arrow"></span></div>',
		nextArrow: '<div  class="slick-next"><span class="icon arrow"></span></div>',
		infinite: true
	});






	$('.menu').on('click', function(){
		$(this).toggleClass('open');
		$('body').toggleClass('menu_open');
		$('.menu_area').fadeToggle(600);
	});

	// hide/show phone mobile version
	$('.m_phone .icon').on('click', function(){
		$('.m_p_phone').fadeToggle(400);
		$('.lang').fadeToggle(100);
	});




	$('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	});


	// clone elements
	$(".contact_area .contact_area_in .contact_right .title").clone()
		.removeClass("title")
		.addClass("m_title")
		.appendTo(".contact_area .contact_area_in .contact_left");

	$(".header_area header .header_left .phone").clone()
		.removeClass("phone")
		.addClass("m_p_phone")
		.appendTo(".header_area header .header_right div.m_phone");

	$(".header_area .book").clone()
		.appendTo(".header_area .slogan");

	$(".events_btn").clone()
		.addClass("events_btn_m")
		.appendTo(".events_area_in");


	$(".footer_left .cop").clone()
		.addClass("cop_m")
		.appendTo(".footer_right");


	$('.way_2').hover(
     function(){ $(".way_1").addClass('hover') },
     function(){ $(".way_1").removeClass('hover') }
	);

	$('.way_3').hover(
     function(){ $(".way_1, .way_2").addClass('hover') },
     function(){ $(".way_1, .way_2").removeClass('hover') }
	);
	$('.way_4').hover(
     function(){ $(".way_1, .way_2, .way_3").addClass('hover') },
     function(){ $(".way_1, .way_2, .way_3").removeClass('hover') }
	);
	$('.way_5, .fishki').hover(
     function(){ $(".way_1, .way_2, .way_3, .way_4").addClass('hover') },
     function(){ $(".way_1, .way_2, .way_3, .way_4").removeClass('hover') }
	);
	$('.way_6').hover(
     function(){ $(".way_1, .way_2, .way_3, .way_4, .way_5").addClass('hover') },
     function(){ $(".way_1, .way_2, .way_3, .way_4, .way_5").removeClass('hover') }
	);



});

// add class whie document is loaded
$(window).load(function() {
	$("body").addClass("loaded");
});



// menu button on page
var menuButton = function() {
	var hpos = $('.menu_l').offset();
	$('.menu').css({"left": hpos.left + 'px', "top": hpos.top + 'px'});
};
$(document).ready(menuButton);
$(window).resize(menuButton);



var WindowsSize = function() {
	var h = $(window).height(),
		w = $(window).outerWidth();
	$("#winSize").html("<p>" + w + " X " + h + "</p>");
};
$(document).ready(WindowsSize);
$(window).resize(WindowsSize);
