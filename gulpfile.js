var gulp = require('gulp');
var stylus = require('gulp-stylus');
var browserSync = require('browser-sync');
var pug = require('gulp-pug');
var notify = require("gulp-notify");
var autoprefixer = require('gulp-autoprefixer');
var SourceMap = require('gulp-sourcemaps');
var rename = require("gulp-rename");
// var uglify = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');
var clean = require('gulp-clean');
var htmlmin = require('gulp-html-minifier');
var cache = require('gulp-cache');
var gulpsync = require('gulp-sync')(gulp);
var zip = require('gulp-zip');
var concat = require('gulp-concat');
var useref = require('gulp-useref');
var gulpif = require('gulp-if');
const imagemin = require('gulp-imagemin');
const size = require('gulp-size');
var svgSprite = require("gulp-svg-sprites");
var removeCode = require('gulp-remove-code');


// Compile stylus,add prefixes, minify css, reload browser and notify
gulp.task('stylus', function() {
  return gulp.src('app/stylus/*.styl') // source folder
    .pipe(SourceMap.init())
    .pipe(stylus().on('error', notify.onError({
      message: "<%= error.message %>",
      title: "Stylus Error!"
    }))) // error notification
    .pipe(autoprefixer({
			browsers: ['last 2 versions'],
      cascade: true,
			grid: true
    })) // add prefix
    .pipe(rename('style.css'))
    .pipe(SourceMap.write('.')) //Create source map
    .pipe(gulp.dest('app/css')) //destination folder
});

// Компилируем Pug в HTML и перегружаем страницу
gulp.task('pug', function() {
  return gulp.src('app/pug/*.pug') // source folder
    .pipe(pug({
      pretty: true
    })) //compile pug
    .pipe(gulp.dest('app')) // destination folder
});

// refresh browser
gulp.task('browser-sync', function() {
  browserSync({
    server: {
      baseDir: 'app'
    },
    notify: false
  })
});

//delete "dist" map
gulp.task('clean', function() {
  return gulp.src(['dist/', 'build.zip'])
    .pipe(clean());
});




//copy images to dist/img
gulp.task('copyimages', function() {
  return gulp.src(['app/img/**/*.*', '!app/img/svg/*.*']) //include all images and exclude img/svg folder
    .pipe(cache(imagemin()))
    .pipe(gulp.dest('dist/img'));
});

// copy fonts to dist/fonts
gulp.task('copyfonts', function() {
  return gulp.src('app/fonts/**/*.*')
    .pipe(gulp.dest('dist/fonts'));
});

//copy html, css, js, change name and path with useref and copy to dist,
gulp.task('copyhtml', function() {
  return gulp.src(['app/*.html', '!app/preview.html']) //include all html file and exclude preview.html

    .pipe(htmlmin({
      collapseWhitespace: false,
      // preserveLineBreaks: true,
      removeComments: false
    }))
    .pipe(removeCode({
      app: true
    }))
    .pipe(useref())
    // .pipe(gulpif('*.js', uglify()))
    // .pipe(gulpif('*.css', minifyCss()))
    .pipe(gulp.dest('dist'));
});


// create archive for build (map "dist")
gulp.task('zip', function() {
	gulp.src('dist/**/*')
		.pipe(zip('build.zip'))
		.pipe(size({
			showFiles: true
		}))
		.pipe(gulp.dest('./'))
});

// svg srpite config
gulp.task('sprites', function () {
	return gulp.src('app/img/svg/*.svg')
		.pipe(svgSprite(
			config = {
				padding: '20',
				cssFile: 'css/sprite.css',
				svg: {
					sprite: 'img/sprite.svg'
				},
				preview: {
					sprite: 'preview.html'
				}
			}
		))
		.pipe(gulp.dest("app/"));
});

//запускаем все команды чтоб прошлись по файлам, потом мониторим все изменения.
gulp.task('default', ['stylus', 'pug', 'browser-sync'],
  function() {
    gulp.watch('app/stylus/**/*.styl', ['stylus-watch']); //watch stylus files
    gulp.watch('app/pug/**/*.pug', ['pug-watch']); //watch pug files
  });

//tasks for relaod page after compile pug and stylus
gulp.task('pug-watch', ['pug'], function(done) {
  browserSync.reload();
  done();
});
gulp.task('stylus-watch', ['stylus'], function(done) {
  browserSync.reload();
  done();
});



//comppile stylus and pug
gulp.task('compile', gulpsync.sync(['stylus', 'pug']));


// build project in "dist map"
gulp.task('build', gulpsync.sync(['compile', 'clean', 'copyhtml',  'copyimages', 'copyfonts', 'zip' ]));
